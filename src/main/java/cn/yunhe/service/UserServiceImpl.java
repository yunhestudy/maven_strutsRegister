package cn.yunhe.service;

import cn.yunhe.dao.UserDao;
import cn.yunhe.dao.daoImpl.UserDaoImpl;
import cn.yunhe.model.User;

public class UserServiceImpl {
	UserDao userDao = new UserDaoImpl();

	public boolean register(User user) {
		return userDao.register(user);
	}

	public User login(User user) {
		return userDao.login(user);
	}
}
