package cn.yunhe.dao;

import cn.yunhe.model.User;

public interface UserDao {
	boolean register(User user);

	User login(User user);
}
