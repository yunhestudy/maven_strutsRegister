package cn.yunhe.dao.daoImpl;

import org.apache.ibatis.session.SqlSession;

import cn.yunhe.dao.UserDao;
import cn.yunhe.factory.SqlSessionFactoryUtil;
import cn.yunhe.model.User;

public class UserDaoImpl implements UserDao {

	public boolean register(User user) {
		SqlSession sqlSession = null;
		int result = 0;
		try {
			sqlSession = SqlSessionFactoryUtil.getSqlSession();
			result = sqlSession.insert("userDao.register", user);
			sqlSession.commit();
		} catch (Exception e) {
			e.printStackTrace();
			sqlSession.rollback();
		} finally {
			if (sqlSession != null) {
				sqlSession.close();
			}
		}
		return result != 0 ? true : false;
	}

	public User login(User user) {
		SqlSession sqlSession = null;
		User userLogin = null;
		try {
			sqlSession = SqlSessionFactoryUtil.getSqlSession();
			userLogin = sqlSession.selectOne("userDao.login", user);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (sqlSession != null) {
				sqlSession.close();
			}
		}
		return userLogin;
	}

}
