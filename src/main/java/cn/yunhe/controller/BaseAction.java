package cn.yunhe.controller;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BaseAction extends ActionSupport{

	private static final long serialVersionUID = 1L;

	// 解耦
	@SuppressWarnings("unchecked")
	public Map<String, Object> getMapRequest() {
		return (Map<String, Object>) ActionContext.getContext().get("request");
	}

	public Map<String, Object> getMapSession() {
		return ActionContext.getContext().getSession();
	}

	public Map<String, Object> getMapApplication() {
		return ActionContext.getContext().getApplication();
	}

	// 耦合
	public HttpServletRequest getRequest() {
		return ServletActionContext.getRequest();
	}

	public HttpSession getSession() {
		return ServletActionContext.getRequest().getSession();
	}

	public HttpServletResponse getReponse() {
		HttpServletResponse reponse = ServletActionContext.getResponse();
		reponse.setContentType("text/html;charset = UTF-8");
		return reponse;
	}
}
