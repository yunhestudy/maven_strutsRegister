package cn.yunhe.controller;

import com.opensymphony.xwork2.ActionSupport;

import cn.yunhe.model.User;
import cn.yunhe.service.UserServiceImpl;

public class UserAction extends ActionSupport {

	private static final long serialVersionUID = 1L;

	private User user;
	BaseAction baseAction = new BaseAction();
	UserServiceImpl userServiceImpl = new UserServiceImpl();

	public String register() {
		baseAction.getMapRequest().put("userName",userServiceImpl.register(user) ? user.getUserName():null);
		return userServiceImpl.register(user) ? SUCCESS : ERROR;
	}

	public String login() {
		baseAction.getMapRequest().put("userName",userServiceImpl.login(user) != null ? user.getUserName():null);
		baseAction.getMapSession().put("userName",userServiceImpl.login(user) != null ? user.getUserName():null);
		return userServiceImpl.login(user) == null ? ERROR : SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
